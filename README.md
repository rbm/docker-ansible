# Ansible docker image

This image is available on [Docker Hub](https://hub.docker.com/r/rbm0407/ansible)

## Base OS
 - Ubuntu focal

## Credits

Based from [William-Yeh/docker-ansible](https://github.com/William-Yeh/docker-ansible) by William Yeh

## License

Licensed under the Apache License V2.0. See the [LICENSE file](LICENSE) for details.
